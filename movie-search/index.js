
// basic setup
var express = require('express');
var request = require('request');

var app = express();

app.set('view engine', 'ejs');

// routes

app.get('/', function (req, res) {
    res.render('search.ejs');
});

app.get('/results', function(req, res){
    // res.send('hello!');
    var  searchQuery = req.query.search;
    var url = 'http://www.omdbapi.com/?apikey=thewdb&s=' + searchQuery; 
    request(url, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var data = JSON.parse(body);
            // res.send(data['Search'][0]['Title']);
            res.render('results.ejs', {data : data}); // res.render('results'); will also work
        }
    });
});


// listeners for port
app.listen(3000, function(){
    console.log('serving movie app on port 3000');
});