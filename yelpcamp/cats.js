var mongoose = require('mongoose');

//connect to db, or create db if it does not exist
mongoose.connect('mongodb://localhost/cat_app');

//define schema
var catSchema = new mongoose.Schema({
    name: String,
    age: Number,
    temprament: String
});


//compile schema into model -- to use create, find functions of mondodb
var Cat = mongoose.model('Cat', catSchema);


// ==============
// add cats to db
// ==============
// var georgie = new Cat({
//     name : 'ms.norris',
//     age: 5,
//     temprament: 'fun'
// });

// //call back function since save, find, remove functions takes time
// georgie.save(function(err, cat){
//     if (err) {
//         console.log('something went wrong!');
//     } else {
//         console.log('we just saved a cat');
//         console.log(cat);        
//     }
// });

Cat.create({
    name: 'bonkers',
    age: 9,
    temprament: 'evil'
}, function(err, cat) {
    if (err) {
        console.log('error');
        console.log(err);
        
    } else {
        console.log(cat);
        
    }
});

//retrieve cats from db and console.log() each one

Cat.find({}, function(err,cats) {
    if (err) {
        console.log('oh no! err');
        console.log(err);
    } else {
        console.log('all the cats');
        console.log(cats);
    }
});