var express = require('express');
var bodyParser = require('body-parser');

var app = express();
global.directory = __dirname; //required to include dev env files for rendering

app.use(bodyParser.urlencoded({extended: true})); // memorize or copy-paste
app.set('view engine', 'ejs');

// // database -- temporary array for db to make local functionality work
// var campgrounds = [
//     {name : 'salmon creek', image: 'https://images.pexels.com/photos/699558/pexels-photo-699558.jpeg?auto=compress&cs=tinysrgb&h=350'},
//     {name : 'granite hill', image: 'https://c1.staticflickr.com/8/7267/6962736430_a1071b9f65_b.jpg'},
//     {name : 'george everest', image: 'https://bonvoyagejogja.com/wp-content/uploads/2017/02/IGjogjacamping.jpg'}
// ]; // this array is static, we will use mongoDB to connect this app to real database

var campgrounds = [
    {name : 'salmon creek', image: 'https://desu-usergeneratedcontent.xyz/g/image/1524/75/1524759147494.jpg'},
    {name : 'granite hill', image: 'https://showcase-thumbs-showcase.netdna-ssl.com/400x300/images.mris.com/image/V3/1/uZaxBwpFTjH7a19h7C1TaPajsF0XHkPX5tuFma-9syTyqLDtSCQ5BAHGDMlW8lh0OwSZ_jUcu1DLSv-d9Rp36A.jpg'},
    {name : 'george everest', image: 'https://www.ehb-designandplanning.co.uk/images/mgthumbnails/400x300-images-houses-IMGP4183.JPG'},
    {name : 'salmon creek', image: 'https://desu-usergeneratedcontent.xyz/g/image/1524/75/1524759147494.jpg'},
    {name : 'granite hill', image: 'https://showcase-thumbs-showcase.netdna-ssl.com/400x300/images.mris.com/image/V3/1/uZaxBwpFTjH7a19h7C1TaPajsF0XHkPX5tuFma-9syTyqLDtSCQ5BAHGDMlW8lh0OwSZ_jUcu1DLSv-d9Rp36A.jpg'},
    {name : 'george everest', image: 'https://www.ehb-designandplanning.co.uk/images/mgthumbnails/400x300-images-houses-IMGP4183.JPG'},
    {name : 'salmon creek', image: 'https://desu-usergeneratedcontent.xyz/g/image/1524/75/1524759147494.jpg'},
    {name : 'granite hill', image: 'https://showcase-thumbs-showcase.netdna-ssl.com/400x300/images.mris.com/image/V3/1/uZaxBwpFTjH7a19h7C1TaPajsF0XHkPX5tuFma-9syTyqLDtSCQ5BAHGDMlW8lh0OwSZ_jUcu1DLSv-d9Rp36A.jpg'},
    {name : 'george everest', image: 'https://www.ehb-designandplanning.co.uk/images/mgthumbnails/400x300-images-houses-IMGP4183.JPG'}
]; // this array is static, we will use mongoDB to connect this app to real database

// routes
app.get('/', function(req, res) {
    // res.send('this is the landing page!');
    res.render('landing');
});

app.get('/campgrounds', function(req, res) {
    res.render('campgrounds', {campgrounds: campgrounds});
});

app.post('/campgrounds', function(req, res) {
    // res.send('you hit the POST route');
    //get data from form and add to campground array
    var name = req.body.name;
    var image = req.body.image;
    var newCampground = {name: name, image: image};
    campgrounds.push(newCampground);
    //redirect back to campgrounds page
    res.redirect('/campgrounds');
});

app.get('/campgrounds/new', function(req, res) {
    res.render('new');
});

// use node_modules to render on server
app.use(express.static('./node_modules')); //uses global.directory = __dirname; to render the node_modules from dev env
app.use(express.static('./images'));

//listeners for the port
app.listen(3000, function(){
    console.log('serving yelpcamp app on port 3000');
});