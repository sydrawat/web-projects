/*
 * Generate Random Number between 1 and 100.
 */

var rNum = Math.floor(Math.random() * 100) + 1;
console.log(rNum);

/*
 * Select HTML elements to manipulate.
 */

var guessField = document.querySelector('.guessField');
var guessSubmit = document.querySelector('.guessSubmit');

var guesses = document.querySelector('.guesses');
var lastResult = document.querySelector('.lastResult');
var lowOrHigh = document.querySelector('.lowOrHigh');

var turns = 1; /* Initialize number of turns to guess the correct random number. */
var resetButton;

/*
 * checkGuess () 
 * Main Logic.  Checking if the number entered by the user is same as the
 * random number generated or not.
 */

function checkGuess() {
    var num = Number(guessField.value);
    if (turns === 1) {
        guesses.textContent = 'Previous Guesses : ';
    }
    guesses.textContent += num + ' ';

    if (num === rNum) {
        lastResult.textContent = 'Correct! Congratulations!';
        lastResult.style.color = 'green';
        lowOrHigh.textContent = '';
        setGameOver();
    }
    else if (turns === 10) {
        lastResult.textContent = 'GAME OVER!';
        lastResult.style.color = 'purple';
        lowOrHigh.textContent = '';
        setGameOver();
    }
    else {
        lastResult.textContent = 'Wrong! Try Again!';
        lastResult.style.color  = 'red';
        if (num < rNum) {
            lowOrHigh.textContent = 'Too Low!';
        }
        else {
            lowOrHigh.textContent = 'Too High!';
        }
    }

    turns ++;
    guessField.value = '';
    guessField.focus();
}

/*
 * Calling checkGuess() when submitGuess button is clicked.
 */

guessSubmit.addEventListener('click',checkGuess);

/*
 * setGameOver()
 * Set the default values for the game once the user has entered the correct
 * answer or has lost all their tries in guessing the random number.
 */

function setGameOver() {
    guessField.disabled = true;
    guessSubmit.disabled = true;
    /*document.createElement('resetButton');*/
    resetButton = document.createElement('button'); /* was wrong -- above */
    resetButton.textContent = 'Reset Game';
    document.body.appendChild(resetButton); /*Missed this line */
    /* center the reset button */
    resetButton.style.width = '50%';
    resetButton.style.margin = 'auto';
    resetButton.style.display = 'block';
    /* END -- center the reset button */
    resetButton.addEventListener('click',resetGame);
}

/*
 * resetGame()
 * Reset whole game settings, clear all inputs and generate a new random 
 * number for the user to guess.
 */

function resetGame() {
    turns = 1;
    var allparas = document.querySelectorAll('.paras p');
    for (var i = 0; i < allparas.length; i++) {
        allparas[i].textContent = '';
    }

    resetButton.parentNode.removeChild(resetButton);

    guessField.disabled = false;
    guessSubmit.disabled = false;

    /* MISSED THESE */
    guessField.value = '';
    guessField.focus();

    lastResult.style.background = 'white';
    /* These three above were missed! */

    rNum = Math.floor(Math.random()*100)+1;
    console.clear();
    console.log(rNum);
}
